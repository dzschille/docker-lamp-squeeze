# LAMP on Debian Squeeze in Docker

To run Docker with non-root access, see "Giving non-root access" on page https://docs.docker.com/installation/ubuntulinux/


## Build the Kombinat LAMP image

	$ docker build -t="kombinat/lamp_img:v5" .


## Start the container

	$ docker run -d -P -v /yourAppHome:/var/www --name magento kombinat/lamp_img:v5

`/yourAppHome/htdocs` is your local folder where the source code is inside. Make sure it is writable for everyone. 
The name of the folder, which will be the webroot, has to be "htdocs" and has to be a first child of "yourAppHome". That means, your web app should be in `/yourAppHome/htdocs`.

Now run `docker ps -a` to look which local port are linked with the containers exposed port 80. The output will look like this:

	CONTAINER ID        IMAGE                  COMMAND                CREATED             STATUS                      PORTS                                          NAMES
	ce25e9f21997        kombinat/lamp_img:v5   /usr/bin/supervisord   2 hours ago         Exited (0) 37 minutes ago   0.0.0.0:49179->22/tcp, 0.0.0.0:49180->80/tcp   magento

Here we see that port 49180 is linked to port 80. So you use the url `http://0.0.0.0:49180` to reach your website.


## Credentials

* the password of the root user is "root"
* the password of the databse root user is not set, but will be set to "root" while running db-full-import-docker.sh (see "your TODO after the first run" at the bottom). If you do not run this script, you should change it manually:

	$ mysqladmin -u root password root


## SSH login possible

If you need to login to the container, you can use ssh. 
`docker ps -a` shows which local port are linked with the containers exposed port 22. Than you use this port to login:

	$ ssh root@0.0.0.0 -p 49179


## Daily start and stops

From now on you can start your container with `docker start magento` and stop it with `docker stop magento`.

