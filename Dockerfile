# Set the base image 
FROM debian:jessie

MAINTAINER David Zschille

# install packages
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y libssl1.0.0 openssl

# install more packages
RUN apt-get update
RUN apt-get install -y apache2 php5 mysql-server php5-mysql php5-mcrypt php5-curl php5-gd php5-imagick php5-xdebug php5-mcrypt vim htop wget openssh-server less supervisor imagemagick libpng-dev libjpeg-dev
RUN a2enmod rewrite php5

# genereate a volume inside the container 
VOLUME [ "/var/www" ]

# configure apache
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
RUN mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR
RUN rm /etc/apache2/sites-enabled/000-default.conf
COPY 100-magento /etc/apache2/sites-enabled/100-magento.conf

# install adminer
RUN mkdir -p /usr/share/adminer
RUN wget "http://www.adminer.org/latest.php" -O /usr/share/adminer/latest.php
RUN ln -s /usr/share/adminer/latest.php /usr/share/adminer/adminer.php
RUN echo 'Alias /adminer.php /usr/share/adminer/adminer.php' | tee /etc/apache2/conf-enabled/adminer.conf

# configure ssh daemon
RUN mkdir -p /var/run/sshd
RUN mkdir -p /var/log/supervisor
RUN echo 'root:root' |chpasswd

RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

# we use supervisord to be able run more than a single process
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

EXPOSE 22 80

CMD ["/usr/bin/supervisord"]

